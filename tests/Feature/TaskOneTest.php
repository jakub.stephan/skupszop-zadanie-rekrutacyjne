<?php

namespace Tests\Feature;

use App\Enums\BookBinding;
use App\Models\Book;
use Database\Seeders\BookSeeder;
use Tests\TestCase;

class TaskOneTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->seed(BookSeeder::class);
        Book::factory()->create([
            'binding' => BookBinding::UNKNOWN
        ]);
    }

    public function testCanSeeUnknownBindingTypeText(): void
    {
        $response = $this->get(route('books.index'));
        $response->assertSee('Nieznana');
    }
}
