<?php

namespace Tests\Feature;

use App\Models\Book;
use Tests\TestCase;

class TaskFiveTest extends TestCase
{
    public function testCanSeeBookEditPage(): void
    {
        $book = Book::factory()->create();
        $response = $this->get(route('books.edit', $book));
        $response->assertStatus(200);
        $response->assertSee('Edycja');
    }
}
