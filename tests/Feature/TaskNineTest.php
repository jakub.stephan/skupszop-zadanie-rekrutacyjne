<?php

namespace Tests\Feature;

use App\Models\Category;
use Database\Seeders\CategorySeeder;
use Tests\TestCase;

class TaskNineTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->seed(CategorySeeder::class);
    }

    public function testCategoriesAreCreatedProperly(): void
    {
        $attributesCount = Category::count();
        $this->assertNotEquals(0, $attributesCount);
    }
}