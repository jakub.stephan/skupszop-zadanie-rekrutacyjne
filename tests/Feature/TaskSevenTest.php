<?php

namespace Tests\Feature;

use Database\Seeders\BookSeeder;
use Tests\TestCase;

class TaskSevenTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->seed(BookSeeder::class);
    }

    public function testCanSeePaginationLinks(): void
    {
        $response = $this->get(route('books.index'));
        $response->assertSee('next');
    }
}
